#include <Adafruit_NeoPixel.h>

#define cellPin A0

#define PIN 2     // input pin Neopixel is attached to

#define NUMPIXELS      8 // number of neopixels in strip

Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

int delayval = 100; // timing delay in milliseconds

int redColor = 0;
int greenColor = 0;
int blueColor = 0;

const float referenceVolts = 5.0; // the default reference on a 5-volt board
const int batteryPin = 0;         // battery is connected to analog pin 0

void setup() {
  //Initialize
  Serial.begin(9600);
  pixels.begin();
}

void loop() {
  //READ VOLTAGE
  int pwr = 0;
  int val = analogRead(batteryPin); // read the value from the sensor
  float volts = (val / 1023.0) * referenceVolts; // calculate the ratio
  //Serial.println(volts); // print the value in volts for testing
  delay(500);
  setColor();
  if(volts>4){
    pwr = 8;
  }else if (volts >3){
    pwr = 6;
  }else if(volts >2){
    pwr = 4;
  }else if(volts >1){
      pwr = 2;
  }else {
    pwr = 1;
  }
  for (int i=0; i < pwr; i++) {
    // pixels.Color takes RGB values
    pixels.setPixelColor(i, pixels.Color(redColor, greenColor, blueColor));

    // Update pixel color to the hardware.
    pixels.show();

    // Delay for a period of time (in milliseconds).
    delay(delayval);
  }
}

//Adjust colour here
void setColor(){
  redColor = 200;
  greenColor = 0;
  blueColor = 0;
}
